var nano = require('nano')('http://localhost:5984');
var dbName = 'alice';
var docName = 'rabbit';

// clean up the database we created previously
// nano.db.destroy(dbName, function() {
  // create a new database
  nano.db.create(dbName, function() {
    // specify the database we are going to use
    var alice = nano.use(dbName);
    // and insert a document in it
    alice.insert({ crazy: true }, docName, function(err, body, header) {
      if (err) {
        console.error('failed insert', err.message);
      } else {
        console.log('inserted', body);
        var rev = body.rev;
        alice.get(docName, { revs_info: true }, function(err, body) {
          if (err) {
            console.error('failed get', err.message);
          } else {
            console.log('read', body);
            alice.destroy(docName, rev, function(err, body) {
              if (err) {
                console.error('failed destroy', err.message);
              } else {
                console.log('destroyed rev, body', rev, body);
                alice.get(docName, { revs_info: true }, function(err, body) {
                  if (err) {
                    console.log('failed get - as expected', err.message);
                  } else {
                    console.error('read what should be destroyed', body);
                  }
                });
              }
            });
          }
        });
      }
    });
  });
// });
