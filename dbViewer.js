// File: dbViewer.js
// Author: Brendan Studds
//
// Module to access the CouchDB views on a database

var central = 'http://115.146.94.208';
var local = 'http://127.0.0.1:5984';


var server = require('nano')(central);
var centralDb = server.use('tweets');

var views = [];

// Find some views (id = _design/XYZ)
/*
centralDb.list({query: "?id=hello"}, function(err, body) {
    if (!err) {
        body.rows.forEach(function (doc) {
            if (doc.id.indexOf('_design') >= 0) {
                views.push(doc.id);
            }
        });
    } else {
        console.log(err);
    }
    
    console.log(views);
});
*/


// Display a view
centralDb.view('queries', 'hashtags', { group:true }, function(err, body) {
    var maxCount = 0;
    var maxName = '';
    if (err) {
        console.error(err);
    } else {
        body.rows.forEach( function (doc) {
            if (doc.value >= maxCount) {
                maxCount = doc.value;
                maxName = doc.key;
            }
        });
    }

    console.log('Most popular hashtag: ' + maxName + '(' + maxCount + ')');
});
