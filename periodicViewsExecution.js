// File: periodicViewsExecution.js
// Author: Brendan Studds
//
// Module that periodically runs all the views.

var RUN_EVERY = 5 * 60 * 1000; // 5 minutes

var database = 'http://127.0.0.1:5984/tweets';
// List of all the design views
var VIEWS = [
    'dayhour',
    'days',
    'geocoded',
    'hashtags',
    'hours',
    'users',
    'profanity',
    'tagtime'
];
var DESIGN_DOC = 'queries';

var getIsoTime = function() {
        return new Date().toJSON().substring(0,19).replace('T',' ');
};

var runViews = function () {
    var tweetsDb = require('nano')(database);
    console.log(getIsoTime() + ' Retrieving views');
    VIEWS.forEach( function (view) {
        tweetsDb.view(DESIGN_DOC, view, { group:true }, function(err, body) {
            if (err) {
                console.log('Error retrieving view\n', err);
            } else {
                console.log(getIsoTime() + ' Retrieved view ' + view);
            }
        });
    });
};

runViews();
var intv = setInterval(runViews, RUN_EVERY);


