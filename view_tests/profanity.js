// Adapted from http://nayuki.eigenstate.org/res/caesar-cipher-javascript.js
function crypt(input, key) {
  var output = "";
  for (var i = 0; i < input.length; i++) {
    var c = input.charCodeAt(i);
    output += String.fromCharCode((c + key) % 256);
  }
  return output;
}

var cusswords = ['P_MU', ']RS^'];
var regexes = [];
cusswords.forEach(function (word) {
  // For RAMs eyes only!
  var decryped = crypt(word, 10);
  regexes[decryped] = new RegExp('\\b' + decryped + '\\b', 'gi');
  console.log(decryped);
});

function map(doc) {
  if (doc.text) {
    for (var word in regexes) {
      var matches = doc.text.match(regexes[word]);
      emit(word, matches.length);
    }
  }
}
