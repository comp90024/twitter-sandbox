// Mocks the emit() function in couchdb.
function emit(key, value) {
  console.log('key: ' + JSON.stringify(key) + ', value: ' + JSON.stringify(value));
}

function log() {
  console.log(JSON.stringify.apply(this, arguments));
}

// Eyes with an optional nose, cannot be joining other characters at the start.
//var stem = '(?<!\\S):\\s*-?\\s*'; // TODO(aramk) For some reason it's not pasting this in Futon...
var stem = '(\\s|^):\\s*-?\s*';
var faces = {
  happy: stem + '\\)',
  sad: stem +'\\(',
  grin: stem + 'D',
  cheeky: stem + 'P',
  shocked: stem + 'O'
};
// To improve performance, we only create the RegExp once.
var _regexCache = {};
for (var face in faces) {
  _regexCache[face] = new RegExp(faces[face], 'gi');
}

function map(doc) {
  var counts = {};
  for (var face in faces) {
    var match = doc.text.match(_regexCache[face]);
    counts[face] = match ? match.length : 0;
  }
  emit(null, counts);
}

function reduce(keys, values, rereduce) {
  return values.reduce(function (prev, curr, i) {
    for (var field in curr) {
      curr[field] += prev[field];
    }
    return curr;
  });
}

map({text: ':)'});
log(reduce(null, [{"happy":1,"sad":0,"grin":0,"cheeky":0}, {"happy":0,"sad":2,"grin":0,"cheeky":1}], true));
