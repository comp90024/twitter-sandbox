// Mocks the emit() function in couchdb.
function emit(key, value) {
  console.log('key: ' + JSON.stringify(key) + ', value: ' + JSON.stringify(value));
}

// Finds if a user is from Newcastle or visiting.
function userFromNewcastle(doc) {
  var loc = doc.user.location;
  var isInName = loc.toLowerCase().indexOf('newcastle') >= 0;
  var rePoint = /(-?\d+\.\d+),(-?\d+\.\d+)/;
  var match = loc.match(rePoint);
  var sensitivity = 0.1;
  // Latitude/Longitude of Newcastle, NSW, Australia
  var realCoords = [-32.928101, 151.772003];
  var isInCoords = !!match && Math.abs(match[1] - realCoords[0]) <= sensitivity && Math.abs(match[2] - realCoords[1]) <= sensitivity;
  return !!loc && (isInName || isInCoords);
}

// map function.
function isVisitor(doc) {
  var loc = doc.user.location;
  return !!loc && !userFromNewcastle(doc) && !/^australia$/i.test(loc);
}

function map(doc) {
  var loc = doc.user.location;
  var state;
  if (!loc) {
    state = null;
  } else if (!userFromNewcastle(doc) && !/^australia$/i.test(loc)) {
    state = 'visitor';
  } else {
    state = 'local';
  }
  emit(state, doc.id);
}

// This turned out to be the wrong approach for CouchDB reduces!
function _reduceOld(keys, values, rereduce) {
  var results = {visitor: 0, local: 0, null: 0};
  if (rereduce) {
    // Combine the results of older reduce() calls
    values.forEach(function (value) {
      for (var field in value) {
        if (field in results) {
          results[field] += value[field];
        }
      }
    });
  } else {
    // Combine the result of a map()
    keys.forEach(function (key) {
      if (key[0] in results) {
        results[key[0]] += 1;
      }
    });
  }
  return results;
}

function reduce(keys, values, rereduce) {
  return sum(values);
}

//console.log(userFromNewcastle({user: {location: ''}}), false);
//console.log(userFromNewcastle({user: {location: '-32.882228,151.697607'}}), true);
//console.log(userFromNewcastle({user: {location: '-32.953,151.72'}}), true);
//console.log(userFromNewcastle({user: {location: 'Newcastle, Australia'}}), true);
//console.log(userFromNewcastle({user: {location: 'London, UK'}}), false);
//
//console.log(isVisitor({user: {location: ''}}), false);
//console.log(isVisitor({user: {location: '-32.882228,151.697607'}}), false);
//console.log(isVisitor({user: {location: '-32.953,151.72'}}), false);
//console.log(isVisitor({user: {location: 'Newcastle, Australia'}}), false);
//console.log(isVisitor({user: {location: 'London, UK'}}), true);

map({user: {location: ''}});
map({user: {location: '-32.882228,151.697607'}});
map({user: {location: '-32.953,151.72'}});
map({user: {location: 'Newcastle, Australia'}});
map({user: {location: 'London, UK'}});