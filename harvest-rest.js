// Note: to get 'twit': npm install twit
var util = require('util'),
    twitter = require('twit'),
    request = require('request'),
    nano = require('nano')('http://localhost:5984');

var twit = new twitter({
    consumer_key: 'U7as5rmAHX28zgkamOQgg',
    consumer_secret: 'Cn5suk7F7bBKQyugzwvnzlsvnpfwNG7udioc9FZoc',
    access_token: '1462235833-uIS1Wd4NdFOn8H5YmarfS9K400WMBU6uI4rszS0',
    access_token_secret: 'g7bQjeiUwoF6B5hlWluTsHO2wv1CYoDgFsaTPwu8l1w'
});

var TWEET_INTERVAL = 60000;
var TWEET_COUNT = 3000;
var TWEET_GEO = '-32.928101,151.772003,15km';
var TWEET_DB = 'tweets';
var oldestTweetId;

nano.db.create(TWEET_DB, function() {
    var db = nano.use(TWEET_DB);

    var dbcallback = function (err, body) {
        if (err) {
            console.log(err);
        }
    };

    var getTweets = function () {
        var args = {
            geocode: TWEET_GEO,
            count: TWEET_COUNT
        };

        if (oldestTweetId) {
            args['max_id'] = oldestTweetId;
        }

        twit.get('search/tweets', args, function (err, reply) {
            if (err) {
                console.log('Error:', err);
            } else {
                var tweets=reply['statuses'];
                console.log('Harvested ' + tweets.length + ' tweets');
		if (tweets.length == 0) {
			oldestTweetId = 0;
		} else {
                	tweets.forEach(function (tweet, index) {
                    	db.insert(tweet, tweet['id'].toString(), dbcallback);
                	});

                	oldestTweetId = tweets[tweets.length-1]['id'];
		}

            }
        });
    };

    getTweets();
    setInterval(getTweets, TWEET_INTERVAL);
});
